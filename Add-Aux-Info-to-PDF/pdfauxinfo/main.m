//
//  main.m
//  pdfauxinfo
//
//  Created by Masayuki Nii on 2015/10/07.
//  Copyright © 2015年 Masayuki Nii. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreServices/CoreServices.h>
#import <Quartz/Quartz.h>
#import <getopt.h>

#define NSStringFromCStr(X) \
[NSString stringWithCString: X encoding: NSUTF8StringEncoding]

void showHelp(void);
void showUsage(void);
void showVersion(void);

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        int ch;
        
        NSString *inputPath = NULL;
        NSString *outputPath = NULL;
        NSString *userPassword = NULL;
        NSString *ownerPassword = NULL;
        bool isPermitPrinting = true;
        bool isPermitCopying = true;
        bool isDebug = false;
        
        /* options descriptor */
        static struct option longopts[] = {
            { "sourcePDFFile",        required_argument,    NULL,    'i' },
            { "convertedPDFFile",   required_argument,    NULL,    'o' },
            { "author",                required_argument,    NULL,    'a' },
            { "creator",            required_argument,    NULL,    'k' },
            { "title",                required_argument,    NULL,    't' },
            { "userPassword",        required_argument,    NULL,    'u' },
            { "ownerPassword",        required_argument,    NULL,    'x' },
            { "permitPrinting",        required_argument,    NULL,    'p' },
            { "permitCopying",        required_argument,    NULL,    'c' },
            { "subject",            required_argument,    NULL,    's' },
            { "keywords",            required_argument,    NULL,    'w' },
            { "encryptionKeyLength",required_argument,    NULL,    'e' },
            { "version",            no_argument,        NULL,    'v' },
            { "debug",                no_argument,        NULL,    'd' },
            { "help",                no_argument,        NULL,    'h' },
            { NULL,                    0,                    NULL,    0 }
        };
        
        NSString *optValue;
        
        NSMutableDictionary *options = [NSMutableDictionary dictionaryWithCapacity:15];
        
        while ((ch = getopt_long(argc, (char * const *)argv,"i:o:a:k:t:u:x:p:c:s:w:e:vdh", longopts, NULL)) != -1)    {
            switch (ch) {
                case 'i':
                    inputPath = [NSStringFromCStr(optarg) stringByExpandingTildeInPath];
                    break;
                case 'o':
                    outputPath = [NSStringFromCStr(optarg) stringByExpandingTildeInPath];
                    break;
                case 'a':
                    options[(NSString *)kCGPDFContextAuthor] = NSStringFromCStr(optarg);
                    break;
                case 'k':
                    options[(NSString *)kCGPDFContextCreator] = NSStringFromCStr(optarg);
                    break;
                case 't':
                    options[(NSString *)kCGPDFContextTitle] = NSStringFromCStr(optarg);
                    break;
                case 'u':
                    userPassword = NSStringFromCStr(optarg);
                    options[(NSString *)kCGPDFContextUserPassword] = userPassword;
                    break;
                case 'x':
                    ownerPassword = NSStringFromCStr(optarg);
                    options[(NSString *)kCGPDFContextOwnerPassword] = ownerPassword;
                    break;
                case 'p':
                    optValue = [NSString stringWithCString: optarg
                                                  encoding: NSUTF8StringEncoding];
                    if ( ( [optValue caseInsensitiveCompare:@"YES"] == NSOrderedSame )
                        || ( [optValue caseInsensitiveCompare:@"TRUE"] == NSOrderedSame )
                        || ( [optValue caseInsensitiveCompare:@"1"] == NSOrderedSame ) )    {
                        isPermitPrinting = true;
                        [ options setValue:    [NSNumber numberWithBool: isPermitPrinting]
                                    forKey:    (void *)kCGPDFContextAllowsPrinting ];
                    }
                    else if ( ( [optValue caseInsensitiveCompare:@"NO"] == NSOrderedSame )
                             || ( [optValue caseInsensitiveCompare:@"FALSE"] == NSOrderedSame )
                             || ( [optValue caseInsensitiveCompare:@"0"] == NSOrderedSame ) )    {
                        isPermitPrinting = false;
                        [ options setValue:    [NSNumber numberWithBool: isPermitPrinting]
                                    forKey:    (void *)kCGPDFContextAllowsPrinting ];
                    }
                    break;
                case 'c':
                    optValue = [NSString stringWithCString: optarg
                                                  encoding: NSUTF8StringEncoding];
                    if ( ( [optValue caseInsensitiveCompare:@"YES"] == NSOrderedSame )
                        || ( [optValue caseInsensitiveCompare:@"TRUE"] == NSOrderedSame )
                        || ( [optValue caseInsensitiveCompare:@"1"] == NSOrderedSame ) )    {
                        isPermitCopying = false;
                        [ options setValue:    [NSNumber numberWithBool: isPermitCopying]
                                    forKey:    (void *)kCGPDFContextAllowsCopying ];
                    }
                    if ( ( [optValue caseInsensitiveCompare:@"NO"] == NSOrderedSame )
                        || ( [optValue caseInsensitiveCompare:@"FALSE"] == NSOrderedSame )
                        || ( [optValue caseInsensitiveCompare:@"0"] == NSOrderedSame ) )    {
                        isPermitCopying = false;
                        [ options setValue:    [NSNumber numberWithBool: isPermitCopying]
                                    forKey:    (void *)kCGPDFContextAllowsCopying ];
                    }
                    break;
                case 's':
                    options[(NSString *)kCGPDFContextSubject] = NSStringFromCStr(optarg);
                    break;
                case 'w':
                    options[(NSString *)kCGPDFContextKeywords]
                    = [NSStringFromCStr(optarg) componentsSeparatedByString:@","];
                    break;
                case 'e':
                    options[(NSString *)kCGPDFContextEncryptionKeyLength]
                    = [NSNumber numberWithInt: NSStringFromCStr(optarg).intValue];
                    break;
                case 'h':
                    showHelp(); return 4; break;
                case 'v':
                    showVersion(); return 4; break;
                case 'd':
                    isDebug = YES; break;
                default:
                    showUsage(); return 5; break;
            }
        }
        
        if ( ( inputPath == NULL ) || ( outputPath == NULL ) )    {
            printf("You have to specify the source PDF file and converted file.\n");
            showUsage();
            return 1;
        }
        if ( ( ownerPassword.length == 0 ) && ( userPassword.length != 0 ) )    {
            [options setValue: @""
                       forKey:    (void *)kCGPDFContextOwnerPassword];
        }
        if ( isDebug )    {
            CFShow((__bridge CFTypeRef)(options));
        }
        NSURL *inputURL = [NSURL fileURLWithPath: inputPath];
        PDFDocument *pdfDoc = [[PDFDocument alloc] initWithURL: inputURL];
        if ( pdfDoc == NULL )    {
            printf("Input File is not valid.");
            return 2;
        }
        if ( ! [pdfDoc writeToFile: outputPath withOptions: options] )    {
            printf("Error in writing to PDF file.");
            return 3;
        }
        return 0;
    }
}

void showUsage()    {
    printf( "Please check the help message by 'pdfauxinfo --help'.\n\n" );
}

void showVersion()  {
    printf( "pdfauxinfo version 3.0 (Build for OS X El Capitan.)\n" );
    printf( "Copyright (C) 2015 Masayuki Nii (nii@msyk.net).\n");
}

void showHelp()    {
    printf("Description:\n");
    printf("  pdfauxinfo can set the auxiliary information to PDF file using PDFKit in Cocoa.\n");
    printf("  This is the version 3.x command line tool for El Capitan or later.\n\n");
    printf("Parameters:\n");
    printf("  --sourcePDFFile, -i Input_File_Path            Original PDF File path.\n");
    printf("  --convertedPDFFile, -o Output_File_Path        Converted PDF File path.\n");
    printf("  --author, -a Author_Name                       Author info of the converted PDF.\n");
    printf("  --creator, -k Creator_Name                     Creator info of the converted PDF.\n");
    printf("  --title, -t Title_of_Document                  Title of the converted PDF.\n");
    printf("  --userPassword, -u User_Password               User Password for the converted PDF.\n");
    printf("  --ownerPassword, -x Owner_Password             Owner Password for the converted PDF.\n");
    printf("  --permitPrinting, -p [YES|true|1|NO|false|0]   Permission for printing the converted PDF.\n");
    printf("  --permitCopying, -c  [YES|true|1|NO|false|0]   Permission for copying the text.\n");
    printf("  --subject, -s Subject                          Subject for the converted PDF.\n");
    printf("  --keyword, -k Keyword,Keyword...               Keywords for the converted PDF, devide by ','.\n");
    printf("  --encryptionKeyLength, -e length               Encryption key length by bit.\n");
    printf("  --debug, -d                                    Debug mode.\n");
    printf("  --help, -h                                     Show this message.\n\n");
    printf("For the option permitPrinting and permitCopying, YES or true or 1 means permitting, \n");
    printf("the others means not permitting.\n\n");
    printf("The encryptionKeyLength option must be a multiple of 8 between 40 and 128.\n");
    printf("If this option is not specified, 40 bit will use to encrypt.\n");
    printf("If this option is set to other than 40, 128 bit RC4 will use to encrypt.\n\n");
    printf("The sourcePDFFile and convertedPDFFile are required.\n\n");
    printf("pdfauxinfo v3.x works just only OS X El Capitan or later.\n\n");
}

