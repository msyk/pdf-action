#!/usr/bin/env sh

#  main.command
#  Add Aux Info to PDF

#  Created by Masayuki Nii on 2015/10/07.
#  Copyright © 2015年 Masayuki Nii. All rights reserved.

# Comment for Ver.1
# Type of input and output needs com.apple.cocoa.path. Isn't that com.apple.applescript.alias-object.pdf?
# When I set it, the script didn't execute at all.

# Comment for Ver.2
# Adopt to Leopard. I have to change some properties, especially type of input/output.
# These are required as string.

# Comment for Ver.3
# Adopt to El Capitan.

LOGFILE=~/Library/Logs/pdfauxinfo.log
OUTFILE=`mktemp -t pdfauxinfo`
INFILE=`cat`

case "$userpassword" in
"")		u=
up=
;;
*)		u="--userPassword"
up=$userpassword
;;
esac
case "$ownerpassword" in
"")		o=
op=
;;
*)		o="--ownerPassword"
op=$ownerpassword
;;
esac
case "$author" in
"")		a=
ap=
;;
*)		a="--author"
ap=$author
;;
esac
case "$title" in
"")		t=
tp=
;;
*)		t="--title"
tp=$title
;;
esac
case "$creator" in
"")		k=
kp=
;;
*)		k="--creator"
kp="$creator"
;;
esac
case "$subject" in
"")		s=
sp=
;;
*)		s="--subject"
sp="$subject"
;;
esac
case "$keywords" in
"")		w=
wp=
;;
*)		w="--keywords"
wp="$keywords"
;;
esac
case "$permitcopying" in
"")		c=;;
-1)	c="--permitCopying NO " ;;
1)	c="--permitCopying YES " ;;
esac
case "$permitprinting" in
"")		p=;;
-1)	p="--permitPrinting NO " ;;
1)	p="--permitPrinting YES " ;;
esac
case "$enc128" in
"")		e=;;
1)	e="--encryptionKeyLength 128 ";;
esac

/usr/local/bin/pdfauxinfo --sourcePDFFile "${INFILE}" --convertedPDFFile ${OUTFILE}.pdf $u "$up" $o "$op" $a "$ap" $t "$tp" $k "$kp" $s "$sp" $w "$wp" $c $p $e

# Logging

DATESTR=`date "+%m/%d %H:%M:%S"`
echo -n ${DATESTR} >> $LOGFILE
echo -n "　/usr/local/bin/pdfauxinfo --sourcePDFFile \"${INFILE}\" --convertedPDFFile ${OUTFILE}.pdf " >> $LOGFILE
echo "$u \"$up\" $o \"$op\" $a \"$ap\" $t \"$tp\" $k \"$kp\" $c $p" >> $LOGFILE

echo "${OUTFILE}.pdf"

exit 0
