#!/bin/sh

ScriptPath=$(cd $(dirname $0);pwd)

#if [ ! -e /tmp/pdfauxinfo ]
#then
#        echo "You have to build pdfaxuinfo first."
#        exit -1
#fi

#if [ ! -e "/tmp/Add Aux Info to PDF.action" ]
#then
#        echo "You have to build Add Aux Info to PDF.action first."
#        exit -1
#fi

#cp /tmp/pdfauxinfo "${ScriptPath}/InstallerRoot/usr/local/bin"
#cp "/tmp/Add Aux Info to PDF.action" "${ScriptPath}/InstallerRoot/Library/Automator"

pkgbuild --root "${ScriptPath}/InstallerRoot" \
	--identifier "net.msyk.addauxinfotopdf" \
	--sign "3rd Party Mac Developer Installer: Masayuki Nii (W3WVRUYJRT)" \
	"${ScriptPath}/AddAuxInfoToPDF.pkg"

